package somepkg.testng.base;

import somepkg.testng.core.DriverBase;
import somepkg.testng.listeners.ScreenshotListener;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import somepkg.testng.constants.Constants;

@Listeners(ScreenshotListener.class)
public class TestBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);

    @BeforeSuite(alwaysRun = true, description = "Init block before all tests")
    public void initAllTests() {
        LOGGER.debug(String.format("Before suite, Thread id: %s", Thread.currentThread().getId()));
    }

    @AfterSuite(alwaysRun = true, description = "Tear down block after all tests")
    public void tearDownAllTest() {
        LOGGER.debug(String.format("After suite, Thread id: %s", Thread.currentThread().getId()));
    }

    @BeforeMethod(alwaysRun = true)
    public void instantiateDriverObject() {
//        DriverBase.instantiateDriverObject();
//        String sessionId = ((RemoteWebDriver) DriverBase.getDriver()).getSessionId().toString();
//        DriverBase.getDriver().manage().deleteAllCookies();
//        DriverBase.getDriver().manage().window().maximize();
//        DriverBase.getDriver().get(Constants.APP_URL);
    }

    @AfterMethod(alwaysRun = true, description = "Tear down after each test with API")
    public void tearDownApiTest()  {
        LOGGER.debug(String.format("After Method, Thread id: %s", Thread.currentThread().getId()));

    }

    @AfterTest(alwaysRun = true)
    public void closeDriverObjects() {
        DriverBase.closeDriverObjects();
    }

}