package somepkg.testng.base;

import io.qameta.allure.Step;
import somepkg.testng.base.Utils.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class WebTest extends TestBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebTest.class);

    private static ThreadLocal<WebDriverFactory> webDriverThreadLocal = new ThreadLocal<>();

    @BeforeTest(alwaysRun = true, description = "Web init before all web tests")
    public void setUpWebTest() {
        LOGGER.debug("Setup web level");
    }

    @AfterTest(alwaysRun = true, description = "Web tear down after all web tests")
    public void tearDownWebTest() {
        LOGGER.debug("Tear down web level");
        LOGGER.debug(String.format("Thread id: %s", Thread.currentThread().getId()));

    }

    @BeforeMethod(alwaysRun = true, description = "Web init before each web test")
    @Step("creating web driver for current thread")
    public void initWebDriver() {
        LOGGER.debug(String.format("Init web driver, thread: %d", Thread.currentThread().getId()));
//        webDriverThreadLocal.set(new WebDriverFactory());
    }

    @AfterMethod(alwaysRun = true, description = "Web tear down after each web test")
    @Step("Closing web driver for current thread")
    public void tearDownWebDriver() {
        LOGGER.debug("Tead down web driver");
//        getWebDriver().quit();
    }

    @Step("Getting webDriver from thread-local variable")
    protected static WebDriver getWebDriver() {
        return webDriverThreadLocal.get().getWebDriver();
    }
}
