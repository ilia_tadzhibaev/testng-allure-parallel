package somepkg.testng.base.Utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URI;

public class WebDriverHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverHelper.class);

    private WebDriverHelper() {}

    // hardcoded solution
    public static WebDriver createDriver(boolean isLocal) {
        LOGGER.debug(String.format("init web drivre, thread: %s", Thread.currentThread().getId()));

        WebDriver webDriver;
        if (isLocal) {
            WebDriverManager.chromedriver().setup();
            webDriver = new ChromeDriver();
        } else {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setBrowserName("chrome");
            capabilities.setVersion("65.0");
            capabilities.setCapability("enableVNC", true);
            capabilities.setCapability("enableVideo", true);

            try {
                webDriver = new RemoteWebDriver(
                        URI.create("http://localhost:4444/wd/hub").toURL(),
                        capabilities
                );
            } catch (MalformedURLException e) {
                throw new RuntimeException("Invalid grid URI");
            }
        }
        return webDriver;
    }
}
