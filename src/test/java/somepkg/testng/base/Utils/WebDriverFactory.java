package somepkg.testng.base.Utils;

import org.openqa.selenium.WebDriver;

public class WebDriverFactory {

    private WebDriver webDriver;

    public WebDriver getWebDriver() {
        if (null == webDriver) {
            webDriver = WebDriverHelper.createDriver(true);
        }
        return webDriver;
    }
}
