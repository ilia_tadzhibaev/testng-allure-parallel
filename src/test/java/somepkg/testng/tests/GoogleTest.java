package somepkg.testng.tests;


import io.qameta.allure.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import somepkg.testng.base.WebTest;

@Epic("Google")
@Feature("Google Search")
public class GoogleTest extends WebTest {

    @BeforeMethod(alwaysRun = true)
    public void initGoogleTest() {
        System.out.println("Thred number:" + Thread.currentThread().getId());

    }

    @Test(groups = {"smoke"})
    @Flaky
    @Description("Searching for fish")
    @Story("Fish Search")
    @Severity(SeverityLevel.CRITICAL)
    @Issues({
            @Issue("MYISSUE-1"),
            @Issue("MYISSUE-2")
    })
    @TmsLink("TMS-123")
    public void searchForFishTest() {
//        //Arrange
//        GooglePage googlePage = new GooglePage(getDriver());
//        //Act
//        GoogleSearchResultPage googleSearchResultPage = googlePage.searchFor("Fish!");
//        List<WebElement> searchResultElements = googleSearchResultPage.getSearchResultElements();
//        //Assert
//        assertThat(searchResultElements.get(0).getText().toLowerCase(), containsString("fish"));
//        assertThat("Purposefully fail this test", true, is(false));
    }


    @Test(groups = {"smoke"})
    @Story("Cheese Search")
    @Issue("ALR-123")
    @TmsLink("TMS-123")
    public void searchForCheeseTest() {
//        //Arrange
//        GooglePage googlePage = new GooglePage(getDriver());
//        //Act
//        GoogleSearchResultPage googleSearchResultPage = googlePage.searchFor("Cheese!");
//        List<WebElement> searchResultElements = googleSearchResultPage.getSearchResultElements();
//        //Assert
//        assertThat(searchResultElements.get(0).getText().toLowerCase(), containsString("cheese"));
    }

    @Test(groups = {"smoke"})
    @Story("Milk Search")
    @Issue("ALR-123")
    @TmsLink("TMS-123")
    public void searchForMilkTest() {
//        //Arrange
//        GooglePage googlePage = new GooglePage(getDriver());
//        //Act
//        GoogleSearchResultPage googleSearchResultPage = googlePage.searchFor("Milk!");
//        List<WebElement> searchResultElements = googleSearchResultPage.getSearchResultElements();
//        //Assert
//        assertThat(searchResultElements.get(0).getText().toLowerCase(), containsString("milk"));
    }


    @Test(groups = {"smoke"})
    @Story("Eggs Search")
    @Issue("ALR-123")
    @TmsLink("TMS-123")
    public void searchForEggsTest() {
//        //Arrange
//        GooglePage googlePage = new GooglePage(getDriver());
//        //Act
//        GoogleSearchResultPage googleSearchResultPage = googlePage.searchFor("Eggs!");
//        List<WebElement> searchResultElements = googleSearchResultPage.getSearchResultElements();
//        //Assert
//        assertThat(searchResultElements.get(0).getText().toLowerCase(), containsString("eggs"));

    }
}
